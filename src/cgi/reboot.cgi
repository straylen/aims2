#!/usr/bin/perl

##############################################
#
# aims2 Post Reboot CGI.
#
# Prevents hosts from going into endless install loops.
#
##############################################

use strict;
use CGI qw(param);
use Net::DNS::Resolver;
use Socket;
use aims2server::conf;
use aims2server::db;
use aims2server::host;
use aims2server::pxe;

print "Content-type: text/html\n\n";
my $resolver = Net::DNS::Resolver->new;
my $response = $resolver->query( $ENV{REMOTE_ADDR} );

my( $HOST, $DOMAIN, $C);

if(!defined($response)) {
 print "No response from DNS servers";
 exit 1;
}

if($resolver->errorstring eq 'NOERROR') {
 my $record;
 foreach $record ($response->answer()) {
	if ($record->type() eq 'PTR') {
		($HOST, $DOMAIN, $C) = split(/\./, $record->ptrdname());
	} 
 }
} else {
 print "DNS error: $resolver->errorstring";
 exit 1;
}

if (!$HOST) {
 print "No hostname obtained from DNS";
 exit 1;
}

$HOST =~ tr/[A-Z]/[a-z]/;

# FIXME: Set all devices to locaboot when hit.

my $dbconn = $aims2conf::db_conn_string;
my $dbuser = $aims2conf::db_user;
my $dbpass = $aims2conf::db_pass;
my %dbattr = %aims2conf::db_conn_attrib;

my $db = aims2db->new();
my $dbh = $db->Connect($dbconn,$dbuser,$dbpass,%dbattr);

my $host = aims2host->new($dbh, "aims2reboot", {});
my $reg = $host->get_device_by_name($HOST);
unless(values %$reg){
   print "$HOST is not registered with aims2. No action taken.";
   exit 1;
}

my $pxe = aims2pxe->new($dbh, "aims2reboot", {});
   $pxe->disable($HOST);

print "$HOST.$DOMAIN.$C installed by AIMS2 at ".localtime()."\n\nDevice set to localboot.";
exit 1;
