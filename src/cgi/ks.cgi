#!/usr/bin/perl -w

##############################################
#
# aims2 Kickstart file renderer.
#
# The kickstart file will only be rendered to the correct device.
#
# FIXME: Log when called.
# FIXME: Update boot timestamp.
#
##############################################

use strict;
use MIME::Base64;
use Socket qw(getaddrinfo getnameinfo);
use aims2server::host;
use aims2server::conf;
use aims2server::db;

# print HTML header.
print "Content-type: text/html\n\n";

# Who's calling?
my $err;
my @addrs;
my $ip = $ENV{REMOTE_ADDR};
( $err, @addrs ) = getaddrinfo($ip);
if ($err){
    print "# aims2 cannot parse ${ip}\n";
    exit 1;
}
my $hostname;
( $err, $hostname ) = getnameinfo( $addrs[0]->{addr} );
if ($err){
    print "# aims2 cannot find host name for ${ip}\n";
    exit 1;
}
my( $HOST, $domain, $c )  = split(/\./, $hostname);
$HOST =~ tr/[a-z]/[A-Z]/;

# Maybe being contacted by the wrong (but still right) interface.
if($HOST =~ /-IPMI$/){
   $HOST = $`;
}

# Thurs 02 Oct 2008
# Devices in LHCb pit
if($HOST =~ /-V$/){
   $HOST = $`;
}

# Connect to the database.
my $dbconn = $aims2conf::db_conn_string;
my $dbuser = $aims2conf::db_user;
my $dbpass = $aims2conf::db_pass;
my %dbattrs = %aims2conf::db_conn_attrib;

my $db = aims2db->new();
my $dbh = $db->Connect($dbconn, $dbuser, $dbpass, %dbattrs);

# Fetch kickstart array
my $host = aims2host->new($dbh, "aims2ks", {});
my $ks = $host->get_host_kickstart($HOST);
my $kickstart = @$ks[1];
if($kickstart){
   print decode_base64($kickstart);
} else {
   print "# aims2 ks cgi error: kickstart for $HOST not found.\n";
}
exit 1;
