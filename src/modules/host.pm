##############################################
package aims2host;
##############################################

use strict;
use IO::File;
use SQL::Abstract;
use MIME::Base64;
use Data::Dumper;

use aims2server::ldap;
use aims2server::db;

1;

##############################################
sub new
##############################################
{
   my($class, $dbh, $user, $conf) = @_;
   my($self) = bless{
      _DB => $dbh,
      _USER => $user,
      _CONF => $conf,
   }, $class;
   
   return $self;
}

##############################################
sub remove
##############################################
{
   my($self, $host) = @_;
   
   # Remove a device from aims2.
   
   unless($host){
      die "Error: Cannot remove undefined host.\n";
   }
   
   $self->remove_host_record($host);
   
   $self;
}

##############################################
sub get_device_by_name
##############################################
{
   my($self, $hostname) = @_;
   
   # Get device informaation using hostname.
   
   my $dbh = $self->{_DB};
                           
   unless($hostname){
      die "Error: No query string provided.\n";
   }      
   $hostname =~ tr/*/%/;
   $hostname =~ tr/[a-z]/[A-Z]/;
   
   # Select device information from database. 
   # REGISTERERED ENABLED BOOTED DISABLED
   my @fields = qw(i.hw h.hostname h.status h.target h.username h.kopts h.synced1 h.synced2 h.synced3 h.noexpiry h.type h.lgcy h.syncedlgcy1 h.syncedlgcy2 h.syncedlgcy3);
   push(@fields, "to_char(h.registered,'yyyy/mm/dd hh24:mi:ss')");
   push(@fields, "to_char(h.enabled,'yyyy/mm/dd hh24:mi:ss')");
   push(@fields, "to_char(h.disabled,'yyyy/mm/dd hh24:mi:ss')");
   push(@fields, "to_char(h.booted,'yyyy/mm/dd hh24:mi:ss')");
   my $fields = join(",",@fields);
   my $sql = "SELECT $fields FROM pxehosts h, hardware i WHERE h.hostname LIKE ? and i.hostname = h.hostname";
   # Like queries are slow. Displaying all host will still take time, 
   # but the database work is over in less than a second. 
   # (it's transporting the big %hosts struct that takes time)
   if($sql eq "%"){
      $sql = "SELECT $fields FROM pxehosts h, hardware i WHERE i.hostname = h.hostname";
   }
   
   my %hosts = ();
   
   eval {
      my $prepare = $dbh->prepare($sql);
         $prepare->execute($hostname);
         $prepare->bind_columns(undef, map{\$_} @fields);
      while (my ($hardware,@host) = $prepare->fetchrow_array){
         (
            $hosts{$hardware}{HOSTNAME},
            $hosts{$hardware}{STATUS},
	    $hosts{$hardware}{PXEBOOT},
	    $hosts{$hardware}{USERNAME},
	    $hosts{$hardware}{KOPTS},
	    $hosts{$hardware}{SYNCED1},
	    $hosts{$hardware}{SYNCED2},
	    $hosts{$hardware}{SYNCED3},
	    $hosts{$hardware}{NOEXPIRY},
      $hosts{$hardware}{TYPE},
      $hosts{$hardware}{LGCY},
      $hosts{$hardware}{SYNCEDLGCY1},
      $hosts{$hardware}{SYNCEDLGCY2},
      $hosts{$hardware}{SYNCEDLGCY3},
	    $hosts{$hardware}{REG},
	    $hosts{$hardware}{ENABLED},
	    $hosts{$hardware}{DISABLED},
	    $hosts{$hardware}{BOOTED},
         ) = @host;
      }
   };      
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to retrieve host information. $@\n";
      }
   }
   \%hosts;

}

##############################################
sub validate_hardware
##############################################
{
 my($self, $hostname, @hardware) = @_;

   unless($hostname){
      die "Error: hostname not provided.\n";
   }
                           
   unless(@hardware){
      die "Error: hardware address not provided.\n";
   }
 
 my $dbh = $self->{_DB};
 
 eval {
    my @foundhosts;
    for my $hw (@hardware){
      my $prepare = $dbh->prepare("SELECT hostname FROM hardware WHERE hostname != ? AND hw = ?");
      $prepare->execute($hostname,$hw);
      while (my (@foundhost) = $prepare->fetchrow_array){
         push (@foundhosts,join(" ",@foundhost));
      }
    }
    if (@foundhosts) {
      die "Hardware address for $hostname already in use by ".join(" ",@foundhosts)."\n"; 
      }
    };
 
 if($@){
      if($@ =~ $dbh->err){
         die "Error: Failed to validate hardware information:\n$@";
      }
   }
}

##############################################
sub get_host_kickstart
##############################################
{
   my($self, $host) = @_;
   
   # Retrive host kickstart file (and meta data)
   
   my $dbh = $self->{_DB};
                           
   unless($host){
      die "Error: No query string provided.\n";
   }      

   # Select device information from database. 
   
   my @fields = qw(hostname ks source username);
   push(@fields, "to_char(lastupdated,'yyyy/mm/dd hh24:mi:ss')");
   my $fields = join(",",@fields);
   my $sql = "SELECT $fields FROM kickstart WHERE hostname = ?";
   
   my @kickstart;
   
   eval {
      my $prepare = $dbh->prepare($sql);
         $prepare->execute($host);
         $prepare->bind_columns(undef, map{\$_} @fields);
         @kickstart = $prepare->fetchrow_array;
   };      
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to retrieve kickstart information.\n";
      }
   }
   
   \@kickstart;
}

##############################################
sub add
##############################################
{
   my($self, $hostname, $device, $kssource, $kopts) = @_;
   
   # Add a device to the aims2 database.

   my $dbh = $self->{_DB};  
   
   my @hardware;
   
   if( uc($hostname) eq uc($device->{'DeviceName'}) ) {
      #
      # Normal case. hostname = device.
      #
      my $interface = $device->{'NetworkInterfaceCards'};
      for my $i (@$interface){
         push(@hardware, $i->{HardwareAddress}) if ($i->{HardwareAddress});
      }
   } else {
      #
      # Presume it was an interface alias.
      # found at $device->{INTERFACES}->{BoundInterfaceCard}->{HardwareAddress}
      #
      my $interfaces = $device->{'Interfaces'};
      for my $i (@$interfaces){
         if( uc($i->{'Name'}) eq uc($hostname) ){
            push(@hardware,  $i->{BoundInterfaceCard}->{HardwareAddress}) if ($i->{BoundInterfaceCard}->{HardwareAddress});
         }
      }
   }

   # Get interfaces addresses from $device.
   unless(@hardware){
      die "Error: No interface data returned from LanDB for $hostname.\n";
   }

   my $kickstart = $self->validate_ks($kssource);
   
   $kopts = $self->validate_kopts($kopts);
   
   $self->validate_hardware($hostname, @hardware); 
   
   $self->insert_host_record($hostname, $kopts);
   
   $self->insert_hw_records($hostname, @hardware);
   

   if($kickstart){
      $self->insert_ks_record($hostname, $kickstart, $kssource);
   }
   
   $self;
   
}

##############################################
sub updateks
##############################################
{
   my($self, $hostname, $source) = @_;
   
   # Update kickstart file in database. sync or use $kickstart;
   
   unless($hostname){
      die "Cannot updateks. No hostname provided.\n";
   }
   $hostname =~ tr/[a-z]/[A-Z]/;
   
   if($source){
      # New kickstart file to get.
      my $kickstart = $self->validate_ks($source);
         my $ks = $self->get_host_kickstart($hostname);
         my $registered = @$ks[0];
         if($registered){
            $self->update_ks_record($hostname, $kickstart, $source);
         } else {
            $self->insert_ks_record($hostname, $kickstart, $source);
         }
   }else{
      # Sync the old one.
      my $ks = $self->get_host_kickstart($hostname);
      my $src = @$ks[2];
      unless($src){
         die "Error: No previous kickstart source path is defined for $hostname.\n";
      }
      if($src eq "Uploaded"){
         die "Error: This kickstart file was uploaded directly.\n";
      }
      my $kickstart = $self->validate_ks($src);   
      $self->update_ks_record($hostname, $kickstart, $src);
   }
   
   $self;
}

##############################################
sub update_ks_record
##############################################
{
   my($self, $hostname, $kickstart, $source) = @_;
   
   # Update kickstart file for HOSTNAME
        
   my $dbh = $self->{_DB};
   
   # Means we have "something"
   my $sql = "UPDATE kickstart SET ks=?,source=?,lastupdated=CURRENT_TIMESTAMP,username=? WHERE hostname = ?";
   eval {
      my $prepare = $dbh->prepare($sql);
         $prepare->bind_param(1, $kickstart); # FIXME: , { ora_type => 112, ora_field => 'ks'});
         $prepare->bind_param(2, $source);
         $prepare->bind_param(3, $self->{_USER});
         $prepare->bind_param(4, $hostname);
         $prepare->execute();
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Cannot update kickstart file for $hostname. $@\n";
      }
   }
   
   $self;

}

##############################################
sub insert_host_record
##############################################
{
   my($self, $hostname, $kopts) = @_;
   
   my $dbh = $self->{_DB};
   
   unless($kopts){
      $kopts = undef;
   }
	
   my @fields = qw(hostname status username kopts registered target synced1 synced2 synced3 lgcy syncedlgcy1 syncedlgcy2 syncedlgcy3);
   my $fields = join(",",@fields);
   my $sql = "INSERT INTO pxehosts ($fields) VALUES (?,0,?,?,CURRENT_TIMESTAMP,'default', 'N','N','N',0,'N','N','N')";
   
   eval {
      my $prepare = $dbh->prepare($sql);
         $prepare->execute($hostname, $self->{_USER}, $kopts);
   };
   if($@){
      if($@ =~ /.HOST_PK/){
         die "Error: $hostname is already registered.\n";
      }
      if($@ =~ $dbh->err){
         die "Database Error: Cannot register $hostname $@.\n";
      }
   }

   $self;

}

##############################################
sub insert_hw_records
##############################################
{
   my($self, $hostname, @hardware) = @_;
   
   # Create hwaddr records.
   
   unless($hostname){
      die "Error: Cannot create hardware entries without hostname.\n";
   }
   unless(@hardware){
      die "Error: Hardware cannot be null.\n";
   }
   
   my $dbh = $self->{_DB};
   
   eval {
      for my $hw (@hardware){
         my $prepare = $dbh->prepare("INSERT INTO hardware (hostname, hw) VALUES (?,?)");
            $prepare->execute($hostname, $hw);
      }
   };
   if($@){       
      if($@ =~ /.HW_PK/){
         die "Error: Hardware address is already registered to another device.\n";
      }
      if($@ =~ $dbh->err){
         die "Database Error: Cannot register hardware for $hostname.\n";
      }
   }
   
   $self;
}

##############################################
sub insert_ks_record
##############################################
{
   my($self, $hostname, $kickstart, $source) = @_;
   
   # Insert kickstart to database.
   
   my $dbh = $self->{_DB};
   
   if( defined($kickstart) ){
      # Means we have "something"
      my $sql = "INSERT INTO kickstart (hostname, ks, source, username, lastupdated) VALUES (?,?,?,?,CURRENT_TIMESTAMP)";
      eval {
         my $prepare = $dbh->prepare($sql);
         $prepare->bind_param(1, $hostname);
         $prepare->bind_param(2, $kickstart);#, { ora_type => 112, ora_field => 'ks'});
         $prepare->bind_param(3, $source);
         $prepare->bind_param(4, $self->{_USER});
         $prepare->execute();
      };
      if($@){
         if($@ =~ /quota exceeded/){
            # ah, shit.
            die "Database Error: Cannot create kickstart record. aims quota exceeded. Contact Linux Support.\n"; 
         }
         if($@ =~ $dbh->err){
            die "Database Error: Cannot create kickstart record $@.\n";
         }
      }
   }
   
   $self;
   
}

##############################################
sub remove_host_record
##############################################
{
   my($self, $hostname) = @_;

   # Delete host records (include hardware and kickstart).

   my $dbh  = $self->{_DB};

   eval {
      my $prepare = $dbh->prepare("DELETE FROM pxehosts WHERE hostname = ?");
         $prepare->execute($hostname);
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Cannot remove $hostname record from pxehosts $@.\n";
      }
   }

   eval {
      my $prepare = $dbh->prepare("DELETE FROM hardware WHERE hostname = ?");
         $prepare->execute($hostname);
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Cannot remove $hostname record from hardware $@.\n";
      }
   }

   eval {
      my $prepare = $dbh->prepare("DELETE FROM kickstart WHERE hostname = ?");
         $prepare->execute($hostname);
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Cannot remove $hostname record from kickstart $@.\n";
      }
   }
 
      
   $self;
}

##############################################
sub permission
##############################################
{
   my($self, $device) = @_;
   
   my($mainuser, $responsible, $landbmanager) = $self->GetOwners($device);
   
   # Connect to ldap (cerndc.cern.ch).
   my $ldapserver = $self->{_CONF}->{'LDAP_SERVER'} ? $self->{_CONF}->{'LDAP_SERVER'} : "cerndc.cern.ch";
   my $ldapuser = $aims2conf::aims2_user ? $aims2conf::aims2_user : "aims";
   my $ldappass = $aims2conf::aims2_pass ? $aims2conf::aims2_pass : die "password undefined";
   my $ldapbase = $self->{_CONF}->{'LDAP_BASE'} ? $self->{_CONF}->{'LDAP_BASE'} : "DC=cern,DC=ch";
               
   # Use LDAP to get CNs for mail addresses.
   my $ldap = aims2ldap->new();
   $ldap->ldap_connect($ldapserver, $ldapuser, $ldappass);
   my $result = $ldap->ldap_search( $ldapbase, "|(mail=$mainuser)(mail=$responsible)(mail=$landbmanager)", ('cn'));
   my @ldapusers = $ldap->fetch_values( $result, ('cn') );
   
   $self->{_LDAP} = $ldap;

   my $permission = 0;
   
   # User is main-user/responsible/landbmanager
   if( grep {$self->{_USER} eq $_} @ldapusers ){
      # FIXME: Log user and method.
      $permission++;
   }

    # User is member of egroup
    unless($permission > 0){
       if( $self->memberof($responsible) ){
          $permission++;
       }
    }

    # User is member of egroup
    unless($permission > 0){
       if( $self->memberof($mainuser) ){
          $permission++;
       }
    }

    # User is member of egroup
    unless($permission > 0){
       if( $self->memberof($landbmanager) ){
          $permission++;
       }
    }

   # aims support
   unless($permission > 0){
      if( $self->memberof($self->{_CONF}->{'EGROUP_AIMSSUPPORT'}) ){
      #if( $self->memberof("it-dep-fio-la-members\@cern.ch") ){
         $permission++;
      }
   }

   # sys admin, machine in 513 or 613 or 9994 (SafeHost) or 9918 (Wigner) or 773 (Network Hub) or 6045 (LHCb containers)?
   unless($permission > 0){
      my $building = $device->{'Location'}->{Building};
      $building =~ s/^0+//; # fank u Peter :)
      $building =~ s/[A-Z]+//;
      if( grep { $building eq $_ } qw(513 613 9994 9918 773 6045) ){
         if( $self->memberof($self->{_CONF}->{'EGROUP_SYSADMINS'}) ){
            $permission++;
         }
      }
   }

   # Is it a mapping we know about?
   unless($permission > 0){
      my(%owner_hack) = %{$self->{_CONF}->{_MAPPINGS}};
      while(my($entry, $list) = each(%owner_hack)){
         if( $mainuser eq $entry || $responsible eq $entry ){
            my @groups = split(",",$list);
            foreach(@groups){
               if($self->memberof($_)){
                  $permission++;
               }
            }
         }
      }
   }
   
   # Explicit deny.
   unless($permission > 0){
      die "Error: Permission denied for $device->{'DeviceName'}.\n";
   }
   
   $self;
   
}

##############################################
sub GetOwners
##############################################
{
   my($self, $device) = @_;
   unless(ref($device) and ref($device) eq "HASH"){
      die "Error: Device struct expected.\n";
   }
   my(@owners);
   push(@owners, $device->{'ResponsiblePerson'}->{Email});
   my $mainuser = $device->{'UserPerson'};
   if($mainuser){
      for my $res ($mainuser){
         push(@owners, $res->{Email})
      }	
   }
   my $landbmanager = $device->{'LandbManagerPerson'};
   if($landbmanager){
      for my $res ($landbmanager){
         push(@owners, $res->{Email})
      }	
   }
   @owners;
}


##############################################
sub memberof
##############################################
{
   my($self, $group) = @_;
   
   my $member = 0;
   
   my $ldap = $self->{_LDAP};

   my $rest = '';
   
   my $ldapbase = $self->{_CONF}->{'LDAP_BASE'} ? $self->{_CONF}->{'LDAP_BASE'} : "DC=cern,DC=ch";

   if (length($group) != 0) {

    ($group,$rest)=split('@',$group);

    my $result = $ldap->ldap_search( $ldapbase, "(&(cn=$self->{_USER})(memberOf:1.2.840.113556.1.4.1941:=CN=$group,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))", ('cn'));
    my @result = $ldap->fetch_values( $result, ('cn') );
   
    for my $user (@result){
       $user =~ s/^CN=//;
       if ( $self->{_USER} eq $user ) { 
          $member = 1;
          last;
       } 
    }
   }   

   $member;
}

##############################################
sub validate_ks
##############################################
{
   my($self, $kickstart) = @_;
   
   # Check somethings about the kickstart.
   # Does it exist?
   # $kickstart = location (FIXME: or file?)
   
   if($kickstart =~ m/^http:/){
      # Fetch the kickstart file by http.
      $kickstart = $self->get_http_kickstart($kickstart);
   }elsif($kickstart =~ m/^\/afs\//){
      # Fetch the kikcstart file from afs
      $kickstart = $self->get_afs_kickstart($kickstart);
   }
   
   # Kickstart is now a FILE
   # FIXME: Check over the file for --isencrypt etc.
   
   # Return the contents of the kickstart (validated).
   $kickstart = encode_base64($kickstart);
      
   $kickstart;
}

##############################################
sub get_http_kickstart
##############################################
{
   my($self, $ks) = @_;

   unless($ks){
      die "Error: Kickstart cannot be null.\n";
   }   

   # FIXME: What about https?
   my $request = HTTP::Request->new(GET => $ks);
   my $ua = LWP::UserAgent->new;
   my $response = $ua->request($request);

   # FIXME: If not a success, be more descriptive (404?, permission denied? etc)
   unless($response->is_success){
      die "Error: Cannot download kickstart from $ks.\n";
   }

   my $kickstart = $response->content;
   
   $kickstart;
}

##############################################
sub get_afs_kickstart
##############################################
{
   my($self, $ks) = @_;

   unless($ks){
      die "Error: Kickstart location cannot be null.\n";
   }   

   my $kickstart;
   
   open(*ks,'<'.$ks) or die "Error: Unable to open $ks: $!.\n";
      read(*ks, $kickstart, -s *ks);
   close(*ks);
   
   $kickstart;
}

##############################################
sub validate_kopts
##############################################
{
   my($self, $kopts) = @_;
   
   # Validate (sanitise) kernel options
   
   #unless( not $kopts){
   #   my(@kopts) = split("\s", $kopts);
   #}
   
   $kopts;
}

##############################################
sub ks_clob
##############################################
{
   # The client gave us their kickstart file, already base64 encoded.
   my($self, $ks) = @_;
   
   
   
   $self;
}
