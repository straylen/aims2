##############################################
package aims2log;
##############################################
#
# Enables database logging for aims2.
#
# SQL> desc log;
# Name					   Null?    Type
# ----------------------------------------- -------- ----------------------------
# LOGTIMESTAMP				   NOT NULL TIMESTAMP(6)
# LOGMSG 				   NOT NULL VARCHAR2(255)
# LOGSERVER				   NOT NULL VARCHAR2(16)
# LOGUSER				   NOT NULL VARCHAR2(16)
#

use strict;

1;

##############################################
sub new
##############################################
{
   my($class, $dbh, $user) = @_;
   my($self) = bless{}, $class;
   $self->initialize($dbh, $user);
   return $self;
}

##############################################
sub initialize
##############################################
{
   my($self, $dbh, $user) = @_;

   $self->{_SERVER} = `hostname -f`; 
   chomp($self->{_SERVER});

   $self->{_DB} = $dbh;
   unless(defined($dbh)){
      die "Error: Logger cannot initialize without a database connection.\n";
   }
   
   $self->{_USER} = $user;
   unless(defined($user)){
      die "Error: Logger cannot initialize without user credentials.\n";
   }

   $self;
}

##############################################
sub log
##############################################
{
   my($self, $status) = @_;

   unless($status){
      die "Error: Log received nothing meaningful.\n";
   }
   
   my $dbh = $self->{_DB};   
   my $user = $self->{_USER};
   my $server = $self->{_SERVER};

   # Log status to the database.
   my $sql = "INSERT INTO log ( logtimestamp, logmsg, loguser, logserver ) values ( CURRENT_TIMESTAMP, ?, ?, ? )";
   eval {
      my $prepare = $dbh->prepare($sql);
         #$prepare->bind_param(1, $status);
	 #$prepare->bind_param(2, $user);
	 #$prepare->bind_param(3, $server);
         $prepare->execute($status, $user, $server);
   };
   # Bail out if there is a problem.
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to log actions. Exiting...\n";
      }
   }
   $self;
}

##############################################
sub ClearOldLogs()
##############################################
{
   # Remove log entries that are older than 1 weeks.
   my($self) = @_;
   my $dbh = $self->{_DB};

   my $sql = "DELETE FROM log WHERE logtimestamp < SYSDATE - 7";
   eval {
      my $prepare->prepare($sql);
      $prepare->execute();
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Cannot archive logs.\n"; 
      }
   }
   $self;
}

##############################################
sub QueryLogs()
##############################################
{
   my($self, $query, $field) = @_;
   unless($query){
      die "Error: Query cannot be null.\n";
   } 
   my $dbh = $self->{_DB};
   ($query) = $self->UntaintValues($query);
   my %log = ();
   my @fields;
   my %row = ();
   my $sql = "SELECT";
   eval {
      my $prepare = $dbh->prepare($sql);
      $prepare->bind_param(1, "%".$query."%");
      $prepare->execute;
      $prepare->bind_columns(undef, map {\$row{$_}} @fields);
      while (my ($time, @logrows) = $prepare->fetchrow_array){
         (
                $log{$time}{MSG},
                $log{$time}{SERVER},
                $log{$time}{USER}
         ) = @logrows;
      }     
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to query logs for $query.\n";
      }
   }
   \%log;
}
