##############################################
package aims2image;
##############################################

use strict;
use Digest::MD5 qw(md5_hex);

1;

##############################################
sub new
##############################################
{
   my($class, $dbh, $user, $conf) = @_;
   my($self) = bless{
      _DB => $dbh,
      _USER => $user,
      _CONF => $conf,
   }, $class;
   
   return $self;
}

##############################################
sub add
##############################################
{
   my($self, $image) = @_;
   
   my $uefi = 0;
   # Add new pxeboot media to aims2.
   
   if( defined($image->{UEFI})) {
      $uefi = 1;
   } 

   my %record = ();
   
   $record{NAME} = $image->{NAME};
   $record{OWNER} = $self->{_USER};
   $record{ARCH} = $image->{ARCH};
   $record{DESC} = $image->{DESC};
   $record{KOPTS} = $image->{KOPTS};
   $record{GROUPS} = $image->{GROUPS}; 
   $record{VMLINUZ} = $image->{VMLINUZ};
   $record{VMLINUZ_FILE} = $image->{VMLINUZ_BLOB};
   {
   use bytes;
   $record{VMLINUZ_SUM} = md5_hex($image->{VMLINUZ_BLOB});
   $record{VMLINUZ_SIZE} = length($image->{VMLINUZ_BLOB});
   }
   $record{INITRD} = undef;
   $record{INITRD_FILE} = undef;
   $record{INITRD_SUM} = undef;
   $record{INITRD_SIZE} = undef;

   $record{UEFI} = $uefi;
      
   if( defined($image->{INITRD}) ){
      $record{INITRD} = $image->{INITRD};
      $record{INITRD_FILE} = $image->{INITRD_BLOB};
      {
      use bytes;
      $record{INITRD_SUM} = md5_hex($image->{INITRD_BLOB});
      $record{INITRD_SIZE} = length($image->{INITRD_BLOB});
      }
   }
   
   $self->insert_pxeboot_record(%record);
   
   $self;
}

##############################################
sub insert_pxeboot_record
##############################################
{
   my($self, %record) = @_;
   
   my $dbh = $self->{_DB};
   
   # FIXME: Better bind behaviour. 
         
   # Prepare to talk to the database.
   my @fields = qw(name owner arch description kopts groups vmlinuz_source vmlinuz_file vmlinuz_sum vmlinuz_size initrd_source initrd_file initrd_sum initrd_size uploaded synced1 synced2 synced3 uefi);#keys %record;
   my $fields = join(",",@fields);
      
   my $sql = "INSERT INTO pxeboot ($fields) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,'N','N','N',?)";
   #die $sql;
   # Create a new record and upload the BLOBS.
   eval {
   
      # Incase we blow our Quota to hell, so prevent AutoCommits.
      $dbh->{AutoCommit} = 0;
      
      my $prepare = $dbh->prepare($sql);
   
         $prepare->bind_param(1, $record{NAME} );
         $prepare->bind_param(2, $record{OWNER} );
         $prepare->bind_param(3, $record{ARCH} );
         $prepare->bind_param(4, $record{DESC} );
         $prepare->bind_param(5, $record{KOPTS} );
         $prepare->bind_param(6, $record{GROUPS} );
         $prepare->bind_param(7, $record{VMLINUZ} );
         $prepare->bind_param(8, $record{VMLINUZ_FILE}, { ora_type=>113, ora_field=>'VMLINUZ_FILE' } );
         $prepare->bind_param(9, $record{VMLINUZ_SUM});
         $prepare->bind_param(10, $record{VMLINUZ_SIZE});
         
         $prepare->bind_param(11, $record{INITRD} );
         $prepare->bind_param(12, $record{INITRD_FILE}, { ora_type=>113, ora_field=>'INITRD_FILE' } );
         $prepare->bind_param(13, $record{INITRD_SUM});
         $prepare->bind_param(14, $record{INITRD_SIZE});
 
         $prepare->bind_param(15, $record{UEFI});
         
         $prepare->execute();
	 $dbh->commit;
   };
   if($@){
   
      
      # Image already exists.
      if($@ =~ /.BOOT_PK/){
         die "Error: Image ".$record{NAME}." already exists.\n";
      }
      
      # We just blew our quota :(
      if($@ =~ /quota exceeded/){
         $dbh->rollback;
         die "Error: Database quota exceeded. Contact Linux.Support\@CERN.CH\n";
      }
      
      # Something unexpected.
      if($@ =~ $dbh->err){
         die "Database Error: Failed to create pxeboot record $record{NAME}.\n".$dbh->errstr;
      }
      
      # invalid identifier = check your fields...
      
   }
   
   $self;
}

##############################################
sub remove
##############################################
{
   my($self, $name) = @_;
   
   # Remove an image from aims2.
   
   unless($name){
      die "Error: Cannot remove. Name is undefined.\n";
   }
   
   $name =~ tr/[a-z]/[A-Z]/;
   $self->remove_pxeboot_record($name);
   
   $self;
}

##############################################
sub remove_pxeboot_record
##############################################
{
   my($self, $name) = @_;
   
   # Remove pxeboot record from the database.
   
   my $dbh = $self->{_DB};
   
   eval {
      my $prepare = $dbh->prepare("DELETE FROM pxeboot WHERE name = ?");
         $prepare->execute($name);
   };   
   if($@){      
      if($@ =~ $dbh->err){
         die "Database Error: Failed to remove $name.\n";
      }
   }
   
   $self;
}

##############################################
sub get_pxeboot_by_name
##############################################
{
   my($self, $name, $arch, $uefi) = @_;
   
   # Get pxeboot information matching $name.
   
   unless($name){
      die "Error: Cannot query. NAME is undefined.\n";
   }
   $name =~ tr/*/%/;
      
   my $dbh = $self->{_DB};
      
   my @fields = qw(name owner arch kopts groups description initrd_source initrd_sum initrd_size vmlinuz_source vmlinuz_sum vmlinuz_size synced1 synced2 synced3 uefi);
   push(@fields, "to_char(uploaded,'yyyy/mm/dd hh24:mi:ss')");
   my $fields = join(", ", @fields);
   my $sql = "SELECT $fields FROM pxeboot WHERE name LIKE ?";
   my @sqlargs;
   my %pxeboot = ();

   push(@sqlargs, $name);

   if(defined($arch)) {
      $sql.=" AND arch LIKE ?";
      push(@sqlargs, $arch);
      }

   if(defined($uefi)) {
      $sql.=" AND uefi = 1"; 
      }

   eval {
      my $prepare = $dbh->prepare($sql);
           
         $prepare->execute(@sqlargs);
         #$prepare->bind_columns(undef, map { \$pxeboot{$_} } @fields);
      while (my ($name,@image) = $prepare->fetchrow_array){
         (
		$pxeboot{$name}{OWNER},
		$pxeboot{$name}{ARCH},
		$pxeboot{$name}{KOPTS},
		$pxeboot{$name}{GROUPS},
		$pxeboot{$name}{DESCRIPTION},
		$pxeboot{$name}{INITRD_SOURCE},
		$pxeboot{$name}{INITRD_SUM},
		$pxeboot{$name}{INITRD_SIZE},
		$pxeboot{$name}{VMLINUZ_SOURCE},
		$pxeboot{$name}{VMLINUZ_SUM},
		$pxeboot{$name}{VMLINUZ_SIZE},
		$pxeboot{$name}{SYNCED1},
		$pxeboot{$name}{SYNCED2},
		$pxeboot{$name}{SYNCED3},
		$pxeboot{$name}{UEFI},
		$pxeboot{$name}{UPLOADED},
                
         ) = @image;
      }
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Cannot retrieve pxeboot information for $name. $@\n";
      }
   }

   \%pxeboot;
}

##############################################
sub permission
##############################################
{
   my($self, $owner, $groups) = @_;
   
   $self->SetLDAP;
   
   my $permission = 0;
   
   # User is owner?
   if( lc($self->{_USER}) eq lc($owner) ){
      $permission++;
   }
   
   # User is member of groups ?
   unless ($permission > 0) {
     my @tmpgroups=split(',',$groups);
     foreach my $group (@tmpgroups) {
       if ($self->memberof($group)) {
        $permission++;
       }
     }
   }
   # User is member of linux support
   unless($permission > 0){
      # FIXME: conf. 
      if( $self->memberof($self->{_CONF}->{'EGROUP_AIMSSUPPORT'}) ){
      #if( $self->memberof("it-dep-fio-la-members\@cern.ch") ){
         $permission++;
      }
   }

   unless($permission > 0){
      die "Error: Cannot remove image. Permission denied.\n";
   }
   
   $self;
}

##############################################
sub upload_permission
##############################################
{
   my($self) = @_;
   
   # Check user has permission to upload.
     
   $self->SetLDAP;
   
   my $permission = 0;
   
   # User is member of linux support
   unless($permission > 0){
      # FIXME: conf. 
      if( $self->memberof($self->{_CONF}->{'EGROUP_AIMSSUPPORT'}) ){
         $permission++;
      }
   }

   # User is member of upload list.
   unless($permission > 0){
      # FIXME: conf. 
      if( $self->memberof($self->{_CONF}->{'EGROUP_UPLOAD'}) ){
         $permission++;
      }
   }
   
   unless($permission > 0){
      die "Error: You do not have permission to upload pxeboot media.\n";
   }
   
   $self;
}

##############################################
sub SetLDAP
##############################################
{
   my($self) = @_;
   
   # Connect to ldap (cerndc.cern.ch).
   # Set connection.
   my $ldapserver = $self->{_CONF}->{'LDAP_SERVER'} ? $self->{_CONF}->{'LDAP_SERVER'} : "cerndc.cern.ch";
   my $ldapuser = $aims2conf::aims2_user ? $aims2conf::aims2_user : "aims";
   my $ldappass = $aims2conf::aims2_pass ? $aims2conf::aims2_pass : die "password undefined";
   my $ldapbase = $self->{_CONF}->{'LDAP_BASE'} ? $self->{_CONF}->{'LDAP_BASE'} : "DC=cern,DC=ch";
               
   my $ldap = aims2ldap->new();
   $ldap->ldap_connect($ldapserver, $ldapuser, $ldappass);
   
   $self->{_LDAP} = $ldap;
   
}

##############################################
sub memberof
##############################################
{
   my($self, $group) = @_;
   
   my $member = 0;
   
   my $ldap = $self->{_LDAP};

   my $rest = '';
   
   my $ldapbase = $self->{_CONF}->{'LDAP_BASE'} ? $self->{_CONF}->{'LDAP_BASE'} : "DC=cern,DC=ch";

   if (length($group) != 0) {

    ($group,$rest)=split('@',$group);

    my $result = $ldap->ldap_search( $ldapbase, "(&(cn=$self->{_USER})(memberOf:1.2.840.113556.1.4.1941:=CN=$group,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))", ('cn'));
    my @result = $ldap->fetch_values( $result, ('cn') );
   
    for my $user (@result){
       $user =~ s/^CN=//;
       if ( $self->{_USER} eq $user ) { 
          $member = 1;
          last;
       } 
    }
   }   

   $member;
}
