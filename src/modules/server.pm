##############################################
package aims2server;
##############################################

use strict;
use DBI;
use DBD::Oracle;
use MIME::Base64;
use Authen::Krb5;
use File::Basename;
use File::Spec;

##############################################
# WATCH OUT, THESE METHODS NEED TO BE DEFINED IN SOAP.PM AS WELL
use subs qw(new SetUser AddImage AddImageAsAttachments AddImageAsHTTP RemoveImage GetImageByName AddHost RemoveHost GetHostByName EnablePXE DisablePXE GetKickstartFile UpdateKickstartFile RemoveKickstartFile EnableLGCY DisableLGCY);
##############################################

# Server Classes.
use aims2server::db;
use aims2server::conf;
use aims2server::landb;
use aims2server::host;
use aims2server::image;
use aims2server::pxe;


use MIME::Entity;
use vars qw(@ISA);
@ISA = qw(SOAP::Server::Parameters);

my $TMP_UPLOAD = "/tmp/aimshttpuploads";
my $safe_filename_characters = "a-zA-Z0-9_.-";

1;

##############################################
sub new
##############################################
{
   my($class, %args) = @_;
   my($self) = bless { _CONF => undef, _DB => undef, _SERVER => undef, _USER => undef }, $class;
   $self->SetServer;
   $self->ConnectToDatabase;
   $self->SetConf;
   $self->SetUser( $args{'username'}, $args{'password'} );
   $self;
}

##############################################
sub SetConf
##############################################
{
   my($self) = @_;

   my $dbh = $self->{_DB};
   unless($dbh){
      die "Error: Database connection has dropped.\n";
   }

   my $conf = aims2conf->new($dbh);
   $self->{_CONF} = $conf->get;
   $self->{_CONF}->{_MAPPINGS} = $conf->get_owner_mappings;

   $self;
}

##############################################
sub ConnectToDatabase
##############################################
{
   my($self) = @_;

   my $dbconn = $aims2conf::db_conn_string ? $aims2conf::db_conn_string : die "Error: Database server undefined.\n";
   my $dbuser = $aims2conf::db_user ? $aims2conf::db_user : die "Error: Database user undefined.\n";
   my $dbpass = $aims2conf::db_pass ? $aims2conf::db_pass : die "Error: Database password undefined.\n";
   my %dbattrs = %aims2conf::db_conn_attrib ? %aims2conf::db_conn_attrib : die "Error: Database attributes undefined.\n";

   my $db = aims2db->new();
   my $dbh = $db->Connect($dbconn, $dbuser, $dbpass, %dbattrs);

   $self->{_DB} = $dbh;

   $self;
}

##############################################
sub SetServer
##############################################
{
   my($self) = @_;

   my $server = `hostname -f`;
   chomp($server);
   $self->{_SERVER} = $server;

   my $client = $ENV{'REMOTE_HOST'};
   $self->{_CLIENT} = $client;

   # FIXME: IP address.
   # FIXME: Data & Time.

### FIXME: apache witrh selinux will not be able to read these !
   # Is the daemon there?
   # unless(-e '/var/lock/subsys/aims/aims2sync.RUNNING'){
   #   die "Error: Synchronisation subsystem is not running.\n";
   #}

   # Is the database there?
   #if(-e '/var/lock/subsys/aims/aims2sync.DBDOWN'){
   #   die "Error: Database not accessible: Try again later.\n";
   #}

   $self;
}


##############################################
sub SetUser
##############################################
{
   my($self, $username, $password) = @_;

   # Authenticate and set the user array.

   # Credentials must be presented.
   unless($username and $password) {
      die "No authentication credentials presented.\n";
   }

   # The password given to us by the client is base64 encoded... we hope.
   $password = decode_base64($password);

   # Kerberos configuration values.
   my $version = $self->{_CONF}->{'KRB_VERSION'} ? $self->{_CONF}->{'KRB_VERSION'} : "aims2_4";
   my $service = $self->{_CONF}->{'KRB_SERVICE'} ? $self->{_CONF}->{'KRB_SERVICE'} : "aims";
   my $kt_file = $self->{_CONF}->{'KRB_KEYTAB'} ? $self->{_CONF}->{'KRB_KEYTAB'} : "/etc/krb5.keytab.aims";

   # Make sure that the $version strings match, and seperate from password.
   if ($password =~ /^$version=(.*)/s) {
       ($version, $password) = split($version."=", $password);
   } else {
      die "ERROR: You are using an out-of-date AIMS client. Please update.\n";
   }

   # Initiate and create a new Auth Context Object.
   Authen::Krb5::init_context();
   my $ac = new Authen::Krb5::AuthContext;
   unless($ac){
      die "Error: Cannot create a new Authentication Context.\n";
   }

   # Resolve a service principal.
   my $sp = Authen::Krb5::sname_to_principal($self->{_SERVER},$service,KRB5_NT_SRV_HST);
   unless($sp){
      die "Error: Cannot resolve to a service principal.\n";
   }

   # Resolve to the keytab file.
   my $kt = Authen::Krb5::kt_resolve("FILE:$kt_file");
   unless($kt) {
      die "Error: Keytab file not available.\n";
   }

   # Try to decrypt the ticket.
   my $t = Authen::Krb5::rd_req($ac, $password, $sp, $kt);
   unless($t) {
      die "Authentication Failure: ", Authen::Krb5::error(), ".\n";
   }

   my $user = $t->enc_part2->client->data;
   unless($user){
      die "Error: Couldn't extract user from ticket.\n";
   }

   undef($ac);
   Authen::Krb5::free_context();

   $self->{_USER} = $user;

   $self;

}


# THIS METHOD IS KEPT FOR BACKWARDS COMPATIBILITY. LATEST CLIENT WILL UPLOAD IMAGES THROUGH
# MIME ATTACHMENTS
##############################################
sub AddImage
##############################################
{
   my($self, %args) = @_;

   # Upload a new image to aims2.

   unless(%args){
      die "Error: Input cannot be null.\n";
   }

   # Name of the image.
   my $name = $args{'name'} ? $args{'name'} : die "Error: No image name was provided.\n";
   $name = $self->validate($name);
   $name =~ tr/[a-z]/[A-Z]/;

   # Short description of the image.
   my $desc = $args{'desc'} ? $args{'desc'} : die "Error: No image description was provided.\n";
   $desc = $self->validate($desc);
   $desc =~ tr/[a-z]/[A-Z]/;

   # Arch of the image.
   my $arch = $args{'arch'};
   unless($arch and grep { $arch eq $_ } qw(i386 i686 x86_64 aarch64 armv7 armv8)){
      die "Error: Bad values provided for arch.\n";
   }

   # Extra bits for an image.
   my $kopts = $args{'kopts'} ? $args{'kopts'} : undef;
   my $groups = $args{'egroups'} ? $args{'egroups'} : undef;
   my $uefi = $args{'uefi'} ? $args{'uefi'}: undef;

   die "Error: No kernel provided.\n" if (!defined($args{'vmlinuz'}));
   die "Error: No kernel file provided.\n" if (!defined($args{'vmlinuz_blob'}));

   my $pxeboot = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $pxeboot->upload_permission;
      $pxeboot->add({
         NAME => $name,
         ARCH => $arch,
         DESC => $desc,
         KOPTS => $kopts,
         GROUPS => $groups,
         INITRD => $args{'initrd'},
         INITRD_BLOB => $args{'initrd_blob'},
         VMLINUZ => $args{'vmlinuz'},
         VMLINUZ_BLOB => $args{'vmlinuz_blob'},
         UEFI => $uefi,
      });

   "Image $name uploaded to aims.";

}

# THIS METHOD IS KEPT FOR BACKWARDS COMPATIBILITY. LATEST CLIENT WILL UPLOAD IMAGES THROUGH
# HTTP UPLOADS
##############################################
sub AddImageAsAttachments
##############################################
{
   my($self, %args) = @_;

   # Blobs to store in DB
   my $vmlinuz_blob = undef;
   my $initrd_blob = undef;

   # Upload a new image to aims2.

   unless(%args){
      die "Error: Input cannot be null.\n";
   }

   # Extract blob attachments
   my $envelope = pop;
   my @parts = @{$envelope->parts};
   my $content_id = undef;
   foreach ( @parts ) {
      $content_id = $_->head->get('Content-id', 0);
      # Remove trailing strings so the comparison is clean
      chomp($content_id);
      if( $content_id eq "<vmlinuz>" ){
         $vmlinuz_blob = $_->bodyhandle->as_string;
      }
      elsif( $content_id eq "<initrd>" ){
         $initrd_blob = $_->bodyhandle->as_string;
      }
   }

   # Name of the image.
   my $name = $args{'name'} ? $args{'name'} : die "Error: No image name was provided.\n";
   $name = $self->validate($name);
   $name =~ tr/[a-z]/[A-Z]/;

   # Short description of the image.
   my $desc = $args{'desc'} ? $args{'desc'} : die "Error: No image description was provided.\n";
   $desc = $self->validate($desc);
   $desc =~ tr/[a-z]/[A-Z]/;

   # Arch of the image.
   my $arch = $args{'arch'};
   unless($arch and grep { $arch eq $_ } qw(i386 i686 x86_64 aarch64 armv7 armv8)){
      die "Error: Bad values provided for arch.\n";
   }

   # Extra bits for an image.
   my $kopts = $args{'kopts'} ? $args{'kopts'} : undef;
   my $groups = $args{'egroups'} ? $args{'egroups'} : undef;
   my $uefi = $args{'uefi'} ? $args{'uefi'}: undef;

   die "Error: No kernel provided.\n" if (!defined($args{'vmlinuz'}));
   die "Error: No kernel file provided.\n" if (!defined($vmlinuz_blob));

   my $pxeboot = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $pxeboot->upload_permission;
      $pxeboot->add({
         NAME => $name,
         ARCH => $arch,
         DESC => $desc,
         KOPTS => $kopts,
         GROUPS => $groups,
         INITRD => $args{'initrd'},
         # Retrieved from MIME attachments
         INITRD_BLOB => $initrd_blob,
         VMLINUZ => $args{'vmlinuz'},
         # Retrieved from MIME attachments
         VMLINUZ_BLOB => $vmlinuz_blob,
         UEFI => $uefi,
      });

   "Image $name uploaded to aims.";

}

# This method depends on upload.cgi to handle HTTP uploads
# We basically send files, randomize its names, return paths so we can indicate the server where to look
# for the blobs before inserting in the DB, same logic as before, but avoiding XML parsing that makes
# memory consumption explode
##############################################
sub AddImageAsHTTP
##############################################
{
   my($self, %args) = @_;

   # Blobs to store in DB
   my $vmlinuz_blob = undef;
   my $initrd_blob = undef;

   # Upload a new image to aims2.

   unless(%args){
      die "Error: Input cannot be null.\n";
   }

   # Name of the image.
   my $name = $args{'name'} ? $args{'name'} : die "Error: No image name was provided.\n";
   $name = $self->validate($name);
   $name =~ tr/[a-z]/[A-Z]/;

   # Short description of the image.
   my $desc = $args{'desc'} ? $args{'desc'} : die "Error: No image description was provided.\n";
   $desc = $self->validate($desc);
   $desc =~ tr/[a-z]/[A-Z]/;

   # Arch of the image.
   my $arch = $args{'arch'};
   unless($arch and grep { $arch eq $_ } qw(i386 i686 x86_64 aarch64 armv7 armv8)){
      die "Error: Bad values provided for arch.\n";
   }

   # Extra bits for an image.
   my $kopts = $args{'kopts'} ? $args{'kopts'} : undef;
   my $groups = $args{'egroups'} ? $args{'egroups'} : undef;
   my $uefi = $args{'uefi'} ? $args{'uefi'}: undef;


   # Validate user-given filenames so we do not get nasty things
   $args{'vmlinuz_filename'} = validate_filename($args{'vmlinuz_filename'});
   $args{'initrd_filename'} = validate_filename($args{'initrd_filename'});

   # We need these paths so we can read blobs and store them in the DB
   # These args were returned from upload.cgi as an intermediate response after successfull upload
   # Do not use user-given filenames as they are, join them with the restrictec tmp path to avoid any tampering
   my $vmlinuz_http_path = $args{'vmlinuz_filename'} ? "$TMP_UPLOAD/$args{'vmlinuz_filename'}" : undef;
   my $initrd_http_path = $args{'initrd_filename'} ? "$TMP_UPLOAD/$args{'initrd_filename'}" : undef;



   if( !-e $vmlinuz_http_path )
   {
      die "Error: $vmlinuz_http_path file not readable.\n";
   }

   if( defined($initrd_http_path) && !-e $initrd_http_path )
   {
      die "Error: $initrd_http_path file not readable.\n";
   }
   open(*VMLINUZ,'<'.$vmlinuz_http_path) or die "Error: Unable to open file - ".$vmlinuz_http_path.": $!.\n";
   {
      use bytes;
      read(*VMLINUZ, $vmlinuz_blob, -s $vmlinuz_http_path);
   }
   close(*VMLINUZ);
   if(!$vmlinuz_blob){
      die "Error: Unable to read file - ".$vmlinuz_http_path.".\n";
      exit 1;
   }

   if (defined($initrd_http_path)) {
      open(*INITRD,'<'.$initrd_http_path) or die "Error: Unable to open file - ".$initrd_http_path.": $!.\n";
      {
         use bytes;
         read(*INITRD, $initrd_blob, -s $initrd_http_path);
      }
      close(*INITRD);
      if(!$initrd_blob){
         die "Error: Unable to read file - ".$initrd_http_path.".\n";
         exit 1;
      }
   }

   die "Error: No kernel provided.\n" if (!defined($vmlinuz_http_path));
   die "Error: No kernel file provided.\n" if (!defined($vmlinuz_blob));

   my $pxeboot = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $pxeboot->upload_permission;
      $pxeboot->add({
         NAME => $name,
         ARCH => $arch,
         DESC => $desc,
         KOPTS => $kopts,
         GROUPS => $groups,
         INITRD => $args{'initrd'},
         # Retrieved from HTTP uploads
         INITRD_BLOB => $initrd_blob,
         VMLINUZ => $args{'vmlinuz'},
         # Retrieved from HTTP uploads
         VMLINUZ_BLOB => $vmlinuz_blob,
         UEFI => $uefi,
      });

   "Image $name uploaded to aims.";

}

##############################################
sub RemoveImage
##############################################
{
   my($self, %args) = @_;

   # Remove (erase) pxeboot media from aims2.

   my $dbh = $self->{_DB};

   my $name = $args{'imagename'} ? $args{'imagename'} : die "Error: Cannot remove pxe media. NAME is undefined.\n";
   $name = $self->validate($name);
   $name =~ tr/[a-z]/[A-Z]/;

   my $image = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );

   # Only the owner and linuxsoft can remove the image.
   my $pxeboot = $image->get_pxeboot_by_name($name);
   my $owner = $pxeboot->{$name}{OWNER};
   my $groups = $pxeboot->{$name}{GROUPS};
   unless($owner){
      die "Error: $name does not exist.\n";
   }
   $image->permission($owner,$groups);
   $image->remove($name);

   "Image $name removed from aims.";
}

##############################################
sub GetImageByName
##############################################
{
   my($self, %args) = @_;

   # Fetch pxeboot entires matching $name.

   my $name = $args{'imagename'} ? $args{'imagename'} : die "Error: Cannot search for pxeboot. NAME is undefined.\n";

   $name = $self->validate($name);
   $name =~ tr/[a-z]/[A-Z]/;

   my $image = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
   my $pxeboot = $image->get_pxeboot_by_name($name);

   $pxeboot;
}

##############################################
sub AddHost
##############################################
{
   my( $self, %args ) = @_;

   # Register a host with aims2.

   my $dbh = $self->{_DB};

   # Clean up input data.
   my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Cannot remove host. Hostname is undefined.\n";
   }
   $hostname = $self->validate($hostname);

   my $kickstart = $self->validate($args{'kickstart'}) ? $args{'kickstart'} : undef;
   my $kopts = $self->validate($args{'kopts'}) ? $args{'kopts'} : undef;

   my $device = $self->GetDevice($hostname);

   # Do not take name from LanDB yet.. we may be dealing with an Interface alias.
   #$hostname = $device->{'DeviceName'};
   $hostname =~ tr/[a-z]/[A-Z]/;

   # Register the device in the aims2 database.
   my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $host->permission($device);
      $host->remove($hostname);
      $host->add($hostname, $device, $kickstart, $kopts);

   # Kickstart CLOB?
   if($args{'ksclob'}){
      $kickstart = $args{'ksclob'};
      $host->insert_ks_record($hostname, $kickstart, "Uploaded");
   }

   "Host $hostname registered with aims.";
}

##############################################
sub RemoveHost
##############################################
{
   my($self, %args) = @_;

   # Remove (de-register) a device from aims2.

   my $dbh = $self->{_DB};

   my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Add host failed. No hostname specified.\n";
   }
   $hostname = $self->validate($hostname);
   $hostname =~ tr/[a-z]/[A-Z]/;

   #my $device = $self->GetDevice($hostname);

   my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
   my $reg = $host->get_device_by_name($hostname);
   unless(values %$reg){
      die "Error: $hostname is not registered with AIMS.\n";
   }
      #$host->permission($device);
      $host->remove($hostname);

   "Host $hostname removed from AIMS.";
}

##############################################
sub GetHostByName
##############################################
{
   my($self, %args) = @_;

   # Return a struct (reference) containing host data.

   my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Cannot fetch host. No hostname provided.\n";
   }
   $hostname = $self->validate($hostname);
   $hostname =~ tr/[a-z]/[A-Z]/;

   my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_DB});
   my($hosts) = $host->get_device_by_name($hostname);

   $hosts;

}

##############################################
sub GetKickstartFile
##############################################
{
   my($self, %args) = @_;

   # Return kickstart to user (if permission).

   my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Unable to get kickstart. No hostname provided.\n";
   }
   $hostname = $self->validate($hostname);
   $hostname =~ tr/[a-z]/[A-Z]/;

   my $device = $self->GetDevice($hostname);

   my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
   my $reg = $host->get_device_by_name($hostname);
   unless(values %$reg){
      die "Error: $hostname is not registered with AIMS.\n";
   }
   $host->permission($device);

   my $kickstart = $host->get_host_kickstart($hostname);

   # hostname ks(encoded) source username lastupdated
   $kickstart;

}

##############################################
sub RemoveKickstartFile
##############################################
{
   die "Sorry, this feature is currently disabled. Use `updateks` instead.\n";
}

##############################################
sub UpdateKickstartFile
##############################################
{
   my($self, %args) = @_;

   # Update the kickstart file.

   my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Unable to get kickstart. No hostname provided.\n";
   }
   $hostname = $self->validate($hostname);
   $hostname =~ tr/[a-z]/[A-Z]/;

   my $kickstart = $self->validate($args{'kickstart'}) ? $args{'kickstart'} : undef;
   my $ksclob = $args{'ksclob'} ? $args{'ksclob'} : undef;

   my $device = $self->GetDevice($hostname);

   my $host = aims2host->new ( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
   my $reg = $host->get_device_by_name($hostname);
   unless(values %$reg){
      die "Error: $hostname is not registered with AIMS.\n";
   }

   $host->permission($device);

   if($ksclob){
      $host->update_ks_record($hostname, $ksclob, "Uploaded");
   }else{
      $host->updateks($hostname, $kickstart);
   }

   "Kickstart updated for $hostname.";
}


##############################################
sub EnableLGCY
##############################################
{
   my($self, %args) = @_;
   my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Unable to enable. No hostname provided.\n";
   }
   $hostname = $self->validate($hostname);
   $hostname =~ tr/[a-z]/[A-Z]/;

   my $device = $self->GetDevice($hostname);

   my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $host->permission($device);
   my $reg = $host->get_device_by_name($hostname);
   unless(values %$reg){
      die "Error: host $hostname is not registered with AIMS.\n";
   }

   my $pxe = aims2pxe->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
   $pxe->enablelgcy($hostname);
   # legacy is for BIOS only, not UEFI !
   $pxe->changetobios($hostname);

   "Host $hostname legacy boot enabled (bios).";
}

##############################################
sub DisableLGCY
##############################################
{

  my($self, %args) = @_;

  my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Unable to disable. No hostname provided.\n";
   }
   $hostname = $self->validate($hostname);
   $hostname =~ tr/[a-z]/[A-Z]/;

   my $device = $self->GetDevice($hostname);

   my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $host->permission($device);
   my $reg = $host->get_device_by_name($hostname);

   unless(values %$reg){
      die "Error: host $hostname is not registered with aims.\n";
   }

   my $pxe = aims2pxe->new(  $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $pxe->disablelgcy($hostname);

   "Host $hostname legacy boot disabled.";
}

##############################################
sub EnablePXE
##############################################
{
   my($self, %args) = @_;

   # Enable the device with pxeboot target.
   my $warn ="";
   my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Unable to enable. No hostname provided.\n";
   }
   $hostname = $self->validate($hostname);
   $hostname =~ tr/[a-z]/[A-Z]/;

   my $name = $args{'imagename'};
   unless($name){
      die "Error: Unable to enable. No pxeboot target provided.\n";
   }

   my $forever = 0;
   my $noexpiry = $args{'noexpiry'};
   if (defined($noexpiry) && $noexpiry =~/^noexpiry$/) {
     $forever = 1;
   }

   my $boottype;

   my $type= $args{'type'};
   if (defined($type)) {
    if ($type =~/^bios$/ || $type =~/^pxe$/) {
      $boottype = 0;
    } elsif ($type =~/^uefi$/) {
      $boottype = 1;
    } elsif ($type =~/^arm64$/) {
      $boottype = 2;
    } else {
      die "Error: allowed boot types are: pxe|bios|uefi|arm64, but: $type specified.\n";
    }
   } else {
     $type = "bios";
     $boottype = 0;
   }

   $name = $self->validate($name);
   $name =~ tr/[a-z]/[A-Z]/;

   my $image = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
   my $pxeboot = $image->get_pxeboot_by_name($name);
   unless( $pxeboot->{$name} ){
      die "Error: pxeboot target $name does not exist.\n";
   }

   my $device = $self->GetDevice($hostname);

   my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $host->permission($device);
   my $reg = $host->get_device_by_name($hostname);
   unless(values %$reg){
      die "Error: host $hostname is not registered with aims.\n";
   }

   my $pxe = aims2pxe->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $pxe->enable($hostname, $name ,$forever, $boottype);
      # legacy is for BIOS only
      if ($boottype != 0) {
        $pxe->disablelgcy($hostname);
      }

    if ($boottype == 1 && ($pxeboot->{$name}{UEFI} != 1 || !defined($pxeboot->{$name}{UEFI}))) {
       $warn="Warning: Non UEFI target selected for UEFI boot";
    }

   "Host $hostname enabled for $name ($type). $warn";
}

##############################################
sub DisablePXE
##############################################
{
   my($self, %args) = @_;

   # Set device to localboot (ie, disable pxe boot)

   my $hostname = $args{'hostname'};
   unless($hostname){
      die "Error: Unable to get kickstart. No hostname provided.\n";
   }
   $hostname = $self->validate($hostname);
   $hostname =~ tr/[a-z]/[A-Z]/;

   my $device = $self->GetDevice($hostname);

   my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $host->permission($device);
   my $reg = $host->get_device_by_name($hostname);
   unless(values %$reg){
      die "Error: host $hostname is not registered with AIMS.\n";
   }

   my $pxe = aims2pxe->new(  $self->{_DB}, $self->{_USER}, $self->{_CONF} );
      $pxe->disable($hostname);

   "Host $hostname set to localboot.";
}

##############################################
sub GetDevice
##############################################
{
   my($self, $hostname) = @_;

   #die "($hostname)";

   # Get device struct from the network database.
   my $landbep = $self->{_CONF}->{'LANDB_EP'} ? $self->{_CONF}->{'LANDB_EP'} : "http://network.cern.ch/NetworkService";
   my $landbproxy = $self->{_CONF}->{'LANDB_PROXY'} ? $self->{_CONF}->{'LANDB_PROXY'} : "https://network.cern.ch/sc/soap/soap.fcgi?v=6";
   my $landbschema = $self->{_CONF}->{'LANDB_SCHEMA'} ? $self->{_CONF}->{'LANDB_SCHEMA'} : "http://www.w3.org/2001/XMLSchema";
   my $landbuser = $aims2conf::aims2_user ? $aims2conf::aims2_user : "aims";
   my $landbpass = $aims2conf::aims2_pass ? $aims2conf::aims2_pass : die "password undefined";

   my $landb = aims2landb->new();
   $landb->build_token($landbep, $landbproxy, $landbschema, $landbuser, $landbpass);
   my $device = $landb->get_device($hostname);

   $device;

}

##############################################
sub validate
##############################################
{
   my( $self, $value ) = @_;

   # allow / for PATHS
   # allow * for wilcards
   # allow numbers
   # allow letters
   # allow _
   # allow -
   # allow :

   # 'clean up' input values
   #if(defined($value) && $value ne ""){
   #   if($value =~ /([\w\-\\\/\*\:\-\=]+)$/){
   #      $value = $1;
   #   } else {
   #     die "Error: Validation is a bit strict.\n";
   #   }
   #}

   return $value;
}

##############################################
sub validate_filename
##############################################
{
   my( $filename ) = @_;

   if ( $filename != undef )
   {
      my ( $name, $path, $extension ) = fileparse ( $filename, '..*' );

      $filename = $name . $extension;
      $filename =~ tr/ /_/;
      $filename =~ s/[^$safe_filename_characters]//g;

      if ( $filename =~ /^([$safe_filename_characters]+)$/ )
      {
         $filename = $1;
      }
      else
      {
         die "$filename contains invalid characters";
      }
   }
   return $filename;
}
