####################################
package aims2www;
####################################

use strict;
use base qw(CGI);
use Carp;
use Data::Dumper;

my $CGI;

1;

####################################
#BEGIN 
####################################
#{
#   $SIG{__DIE__} = sub {
#	my($error) = $_[-1]; 
#	DieNice($error);  
#  };   
#}

####################################
sub new
####################################
{
   my($class) = shift;
   my($self) = bless {}, $class;
   $CGI = new CGI;
   print $CGI->header;
   $self;
}

####################################
sub DieNice
####################################
{
   my($status) = @_;
   print "Content-type: text/html\n\n";
   print "<hr class=\"space\" /><p class=\"error\"><b>Fatal Error</b><br />".$status."</p>";
   exit 1;
}

####################################
sub get_query_string
####################################
{
   my($self) = shift;
   
   my($method, $pair, $buffer, %filter);
   
   if($ENV{'REQUEST_METHOD'} eq "POST") {
      read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
   } else {
      $buffer = $ENV{'QUERY_STRING'};
   }
   my @pairs = split(/&/,$buffer);
   foreach $pair (@pairs) {
      my ($key,$value) = split(/=/,$pair);

      next if!$value;

      $value =~ tr/+/ /;
      $value =~ tr/*/%/;

      $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;

      $filter{$key} = $value;

      $method = $filter{'Action'};

   }

   delete($filter{'Action'});

   ( $method, %filter );
}

####################################
sub print_hosts
####################################
{
   my($self, $hosts) = @_;
   unless(ref($hosts) and ref($hosts) eq "HASH")
   {
      die "Error: Result was not a reference.";
   }  
   for my $hw (keys %$hosts){
      my $hostname = $hosts->{$hw}->{'HOSTNAME'};
      my $pxestatus = $hosts->{$hw}->{'PXE'};
      my $pxeboot = $hosts->{$hw}->{'PXEBOOT'};
      if($pxestatus == 0){
        $pxestatus = "Disabled";
        $pxeboot = "n/a";
      }else{
        $pxestatus = "Enabled";
      }
      print $CGI->div({-class=>"span-20 record"},
         $CGI->div({-class=>"span-5"}, $CGI->a({-href=>"host.htm?hostname=".$hostname}, $hostname)),
         $CGI->div({-class=>"span-5"}, $hw),
         $CGI->div({-class=>"span-5"}, $pxestatus),
         $CGI->div({-class=>"span-5 last"}, $pxeboot)
      );

   }
   $self;
}

####################################
sub print_images
####################################
{
   my($self, $images) = @_;
   unless(ref($images) and ref($images) eq "HASH")
   {
      die "Error: Result is not a reference.";
   }
   for my $name (keys %$images){
      my $arch = $images->{$name}->{ARCH} || "Unknown";
      my $kopts = $images->{$name}->{KOPTS} || "No boot options specified";
      print $CGI->div({-class=>"span-20 record"}, 
         $CGI->div({-class=>"span-5"}, $CGI->a({-href=>"image.htm?imagename=$name"}, $name)),
         $CGI->div({-class=>"span-5"}, $arch),
         $CGI->div({-class=>"span-10 last"}, $kopts),
      );
   }
   $self;
}

####################################
sub print_host	
####################################
{
   my($self, $host) = @_;
   unless(ref($host) && ref($host) eq "HASH")
   {
      print $CGI->p({-class=>"error"}, "Error: Database did not return a host reference.");
      exit 1;
   }

   # Build a new HOST{hash}.
   my @hardware;
   my %HOST = ();
   for my $hw (keys %$host){
      chomp($hw);
      push(@hardware, $hw);
      $HOST{'HOSTNAME'} 	= $host->{$hw}->{HOSTNAME};
      $HOST{'HARDWARE'} 	= join(", ",@hardware); #@hardware];
      $HOST{'KOPTS'} 		= $host->{$hw}->{KOPTS} || "No host boot options specified.";
      $HOST{'KICKSTART'} 	= $host->{$hw}->{KICKSTART} || "No kickstart file for this host.";
      $HOST{'REGISTERED'} 	= $host->{$hw}->{REGISTERED} || "Error: Registration date unknown.";
      $HOST{'PXEBOOT'} 		= $host->{$hw}->{PXEBOOT} || "n/a";
      $HOST{'STATUS'} 		= $host->{$hw}->{PXESTATUS};
      $HOST{'REGISTEREDBY'} 	= $host->{$hw}->{REGISTEREDBY} || "Unknown."; 
      $HOST{'ENABLED'} 		= $host->{$hw}->{ENABLED} || "Device has not been enabled.";
      $HOST{'DISABLED'} 	= $host->{$hw}->{DISABLED} || "Device has yet to be disabled.";
      $HOST{'KSRENDERED'}	= $host->{$hw}->{KSRENDERED} || "Kickstart not served. Host unlikely to have booted yet.";
   }

   # Decode the kickstart file.
   use MIME::Base64;
   $HOST{'KICKSTART'} = decode_base64($HOST{'KICKSTART'});
   if($HOST{'STATUS'}){
      $HOST{'STATUS'} = '<span class="enabled">Enabled</span>';
   } else {
      $HOST{'STATUS'} = '<span class="disabled">Disabled</span>';
   }

   # Host/Device Information.
   print $CGI->h3($HOST{'HOSTNAME'});
   print $CGI->a({-href=>"https://network.cern.ch/sc/fcgi/sc.fcgi?Action=SearchForDisplay&DeviceName=".$HOST{'HOSTNAME'},-target=>"blank"},"Show host in Network 
Database.");
   print $CGI->p({-class=>"box"}, "<b>Boot Options</b>:<br />".$HOST{'KOPTS'});
   print $CGI->div({-class=>"span-20 showhost"}, 
	$CGI->div({-class=>"span-10"}, "<b>Interfaces</b>:"),
  	   $CGI->div({-class=>"span-10 last"}, $HOST{'HARDWARE'}),
	$CGI->div({-class=>"span-10"}, "<b>Host Registered</b>:"),
	   $CGI->div({-class=>"span-10 last"}, $HOST{'REGISTERED'}),
	$CGI->div({-class=>"span-10"}, "<b>Registered by</b>:"),
 	   $CGI->div({-class=>"span-10 last"}, $HOST{'REGISTEREDBY'}),
	$CGI->div({-class=>"span-10"}, "<b>PXE Status</b>:"),
	   $CGI->div({-class=>"span-10 last"}, $HOST{'STATUS'}),
	$CGI->div({-class=>"span-10"}, "<b>PXEBoot</b>:"),
	   $CGI->div({-class=>"span-10 last"}, $HOST{'PXEBOOT'}),
	$CGI->div({-class=>"span-10"}, "<b>Host Enabled</b>:"),
	   $CGI->div({-class=>"span-10 last"}, $HOST{'ENABLED'}),
	$CGI->div({-class=>"span-10"}, "<b>Host Booted</b>:"),
	   $CGI->div({-class=>"span-10 last"}, "-"),
	$CGI->div({-class=>"span-10"}, "<b>Kickstart Request</b>:"),
	   $CGI->div({-class=>"span-10 last"}, $HOST{'KSRENDERED'}),
	$CGI->div({-class=>"span-10"}, "<b>Deregistered</b>:"),
	   $CGI->div({-class=>"span-10 last"}, $HOST{'DISABLED'}),
   );

  # Decoded Kickstart contents.
  print $CGI->textarea({-class=>"kickstart", -readonly=>"yes", -rows=>25, -default=>$HOST{'KICKSTART'}});
  print $CGI->hr({-class=>"space"});

  # User Actions.
  print $CGI->button("Enable PXE");
  print $CGI->button("Disable PXE");
  print $CGI->button("Remove Host");
  print $CGI->button("Edit Host");
  print $CGI->button("View Log for ".$HOST{HOSTNAME});
}

####################################
sub print_image	
####################################
{
   my($self, $image) = @_;
   unless(ref($image) and ref($image) eq "HASH")
   {
      print $CGI->p({-class=>"error"}, "Error: Database did not return an image reference.");
      exit 1;      
   }
   for my $name (keys %$image){
#      print $image->{$name}->{INITRD};
#      print $image->{$name}->{VMLINUZ};

       print $CGI->h2($name);

       print $CGI->p({-class=>"box"}, $image->{$name}->{DESCRIPTION});

       print $CGI->div({-class=>"span-20 showimage"}, 
          $CGI->div({-class=>"span-10"}, "<b>Arch</b>:"),
    	     $CGI->div({-class=>"span-10 last"}, $image->{$name}->{ARCH} || "Unknown."),

          $CGI->div({-class=>"span-10"}, "<b>Uploaded</b>:"),
    	     $CGI->div({-class=>"span-10 last"}, $image->{$name}->{CREATED}),

          $CGI->div({-class=>"span-10"}, "<b>Owner</b>:"),
    	     $CGI->div({-class=>"span-10 last"}, $image->{$name}->{OWNER}),

          $CGI->div({-class=>"span-10"}, "<b>Boot Options</b>:"),
    	     $CGI->div({-class=>"span-10 last"}, $image->{$name}->{KOPTS} || "No boot options defined."),

          $CGI->div({-class=>"span-10"}, "<b>e-groups</b>:"),
    	     $CGI->div({-class=>"span-10 last"}, $image->{$name}->{EGROUPS} || "No e-groups defined."),

       );
   }

  print $CGI->hr({-class=>"space"});

  # Dependant hosts.
 # print $CGI->h3("Dependent hosts");
 # print $CGI->scrolling_list('list_name',[%$hosts],['eenie','moe'],5,'true');

  # User Actions.
  print $CGI->hr({-class=>"space"});
  print $CGI->button("Remove Image");
  print $CGI->button("Update Image");


}

