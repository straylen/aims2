##############################################
package aims2landb;
##############################################

use strict;
use SOAP::Lite;

1;

##############################################
sub new
##############################################
{
   my($class) = @_;
   my($self) = bless {}, $class;
   return $self;
}

##############################################
sub build_token
##############################################
{
   my($self, $endpoint, $proxy, $schema, $user, $pass) = @_;

   unless($endpoint && $proxy && $schema && $user && $pass){
      die "Error: build_token failed. Some configuration values undefined.\n";
   }
 
   my $conn = SOAP::Lite->uri($endpoint)
      ->xmlschema($schema)
      ->proxy($proxy, keep_alive => 1 );
      
   unless($conn){
      die "Error: Connection to Network Database failed.\n";
   }
      
   my $call = $conn->getAuthToken( $user, $pass, 'NICE' );
   if ($call->fault) {
      die "Error: Fault getting token for Network Database.\n";
   }
   my ($auth) = $call->result;
   
   my $AuthToken = SOAP::Header->name( 'Auth' => { "token" => $auth } );
   $self->{_TOKEN} = $AuthToken;
   
   $self->{_LANDB} = $conn;
   
}

##############################################
sub get_device
##############################################
{

   my($self, $host) = @_;
   
   # Fetch a device struct from database.
   
   unless($host){
   	die "Error: Cannot query for device without input.\n";
   }
   
   $host =~ tr/[a-z]/[A-Z]/;
   
   my $conn = $self->{_LANDB};
   my $token = $self->{_TOKEN};
   
   my $res1 = $conn->searchDevice($token, { Name => $host });
   if($res1->fault){
      die "Error: Device \"$host\" not found in the Network Database.\n";
   }
   my $dev1 = $res1->result->[0];
   
   my $result = $conn->getDeviceInfo($token, $dev1);
   if($result->fault){
      die "Error: Cannot get device information for \"$host\".\n";
   }
   
   my $device = $result->result;
   
   $device;
   	
}

##############################################
sub get_hardware
##############################################
{
	
   my($self, $device) = @_;
   
   # Fetch hardware address(es) from database.
   
   unless(ref($device) and ref($device) eq "HASH"){
      $device = $self->get_device($device);
   }
   my(@hardware);
   my $interface = $device->{'NetworkInterfaceCards'};
   for my $res (@$interface){
      push(@hardware, $res->{HardwareAddress});
   }
   @hardware;
}

##############################################
sub get_owners
##############################################
{
	
   my($self, $device) = @_;
   
   # Fetch main-user/responsible/landbmanager from database.
   
   unless(ref($device) and ref($device) eq "HASH"){
      $device = $self->get_device($device);
   }
   my(@owners);
   push(@owners, $device->{'ResponsiblePerson'}->{Email});
   my $mainuser = $device->{'UserPerson'};
   if($mainuser){
      for my $res ($mainuser){
         push(@owners, $res->{Email})
      }	
   }
   my $landbmanager = $device->{'LandbManagerPerson'};
   if($landbmanager){
      for my $res ($landbmanager){
         push(@owners, $res->{Email})
      }	
   }	
   @owners;
}

##############################################
sub get_building
##############################################
{
   my($self, $device) = @_;
   
   # Fetch building from database.
   
   unless(ref($device) and ref($device) eq "HASH"){
      $device = $self->get_device($device);
   }	
   my $building = $device->{'Location'}->{Building};
   unless($building){
      die "Error: Undefined location for $device->{'DeviceName'} in network database.\n";
   }
   $building;	
}
