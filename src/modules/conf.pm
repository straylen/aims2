##############################################
package aims2conf;
##############################################

#use strict;
#use vars qw(db_conn_string db_user db_pass db_conn_attrib);
use FileHandle;

# FIXME: Logging. We should log when a configuration key/value is removed, updated or created.
# FIXME: Authen/Authz. Checks on who runs this (should only be admins).

1;

##############################################
# Static Database Connection Values
##############################################
our($db_conn_string,$db_user,$db_pass,$aims2sync_mask,$aims2sync_which,$aims2_server,$aims2_user,$aims2_pass) = &read_conf;
our %db_conn_attrib = (
   'InactiveDestroy'       => 1,
#   'LongReadLen'           => 40_000_000, # Review this more "scientifically" at some point.
   'LongReadLen'	   => 1_000_000_000, # Well DAN!@!##$@$@%! this MUST BE SAME AS UPLOAD LIMIT !!! Jarek
   'AutoCommit'            => 1, # disabled for BLOBS.
   'RaiseError'            => 1,
   'PrintError'            => 1,
   'LongTruncOk'           => 0, 
   'ora_module_name' 	   => $0
);

##############################################
sub new
##############################################
{
   my($class, $dbh) = @_;
   my($self) = bless{}, $class;
   $self->{_DB} = $dbh;
   return $self;
}

##############################################
sub read_conf
##############################################
{
   my $conf = new FileHandle;
   $conf->open("/etc/aims2.conf") or die "Error: Missing configuration. $!\n";
   my %dbconf = (); 
   while(my $l = $conf->getline()){
      chomp($l);
      next if $l =~ m{^#}; 
      my($key,$value) = split("=", $l);
      next if (!defined($key));
      $dbconf{$key} = $value;
   }
   $conf->close();
   ( $dbconf{DB}, $dbconf{User}, $dbconf{Pass}, $dbconf{SyncMask}, $dbconf{SyncWhich}, 
     $dbconf{AimsServer}, $dbconf{AimsUser}, $dbconf{AimsPass} );
}

##############################################
sub get_owner_mappings
##############################################
{
   my($self) = @_;
   
   my %mappings = ();
   
   my $dbh = $self->{_DB};
   
   eval {
      my $prepare = $dbh->prepare("SELECT owner,egroups FROM owners");
         $prepare->execute();
         $prepare->bind_columns(undef, \$owner, \$egroups);
      while($prepare->fetch()){
         $mappings{$owner} = $egroups;
      }
   };
   if($@){
      if($@ =~ /table or view does not exist/){
         # we is in the shit now.
         die "Database Error: Device mappings do not exist.\n";
      }
      if($@ =~ $dbh->err){
         die "Database Error: Cannot retreive device mapppings. $@\n";
      } 
   }

   \%mappings;
}

##############################################
sub get
##############################################
{
   my($self) = @_;
   my( %conf, $key, $value );
   my $dbh = $self->{_DB};   
   my $sql = "SELECT key, value FROM conf";
   eval {
      my $prepare = $dbh->prepare($sql);
         $prepare->execute();
         $prepare->bind_columns(undef, \$key, \$value);
      while($prepare->fetch()){
         $conf{$key} = $value;
      }
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Cannot get server configuration.\n";
      } 
   }
   
   \%conf;
}

##############################################
sub get_value
##############################################
{
   my($self, $key) = @_;
   
   unless($key){
      die "Error: Configuration key required.\n";
   }
   
   $key =~ tr/[a-z]/[A-Z]/;
   
   my %conf = $self->get;
   my $value = $conf{$key};
   $value || undef;
}

##############################################
sub update
##############################################
{
   my($self, $key, $value) = @_;
   unless($key && $value){
      die "Error: Configuration update requires key and value.\n";
   }
   my $dbh = $self->{_DB};
   $key =~ tr/[a-z]/[A-Z]/;
   $key =~ tr/ /_/;
   eval {
      my $prepare = $dbh->prepare("UPDATE conf SET conf_value=? WHERE conf_key = ?");
	 $prepare->bind_param(1, $value);
	 $prepare->bind_param(2, $key);
         $prepare->execute;
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Unable to update configuration value.\n";
      }
   }
   $self;

}

##############################################
sub create
##############################################
{
   my($self, $key, $value) = @_;
   unless($key && $value){
      die "Error: Configuration create requires key and value.\n";
   }
   chomp($key);
   chomp($value);
   $key =~ tr/[a-z]/[A-Z]/;
   $key =~ tr/ /_/;
   my $dbh = $self->{_DB};
   eval {
      my $prepare = $dbh->prepare("INSERT INTO conf (conf_key, conf_value) VALUES (?, ?)");
         $prepare->bind_param(1, $key);
	 $prepare->bind_param(2, $value);
	 $prepare->execute;
   };
   if($@){
      # Is the key a duplicate?
      if($@ =~ /.CONF_PK/){	 
         die "Database Error: Key with that name already exists.\n";
      } 
      if($@ =~ $dbh->err){
         die "Database Error: Failed to create new configuration key/value.\n";
      }
   }
   $self;
}

##############################################
sub remove
##############################################
{
   my($self, $key) = @_;
   unless($key){
      die "Error: Configuration removal requires key and value.\n";
   }   
   my $dbh = $self->{_DB};
   $key =~ tr/[a-z]/[A-Z]/;
   # FixMe: Should check that $user == admin.
   eval {
      my $prepare = $dbh->prepare("DELETE FROM conf WHERE conf_key = ?");
         $prepare->bind_param(1, $key);
         $prepare->execute;
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to remove configuration value $key.\n";
      }
   }
   $self;
}

