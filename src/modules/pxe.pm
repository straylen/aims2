##############################################
package aims2pxe;
##############################################

use strict;

1;

##############################################
sub new
##############################################
{
   my($class, $dbh, $user, $conf) = @_;
   my($self) = bless{
      _DB => $dbh,
      _USER => $user,
      _CONF => $conf,
   }, $class;
   
   return $self;
}

##############################################
sub enable
##############################################
{
   my( $self, $host, $pxeboot, $forever, $boottype ) = @_;
   
   # Enable the target host for installation with pxeboot

   my $noexpiry="noexpiry=0";
   my $type="type=$boottype";

   unless($host){
      die "Error: No targer hostname specified.\n";
   }   
   $host =~ tr/[a-z]/[A-Z]/;

   unless($pxeboot){
     die "Error: No pxeboot target specified.\n";
   }
   $host =~ tr/[a-z]/[A-Z]/;

   if ($forever) { $noexpiry="noexpiry=1"; }

   my $dbh = $self->{_DB};   
   my $sql = "UPDATE pxehosts SET target=(SELECT name FROM pxeboot WHERE name = ? and rownum=1), status=1,enabled=CURRENT_TIMESTAMP,disabled=null,booted=null, syncedlgcy1='N',syncedlgcy2='N',syncedlgcy3='N',synced1='N',synced2='N',synced3='N',".$noexpiry.",".$type." WHERE hostname = ?";
   eval {
      my $prepare = $dbh->prepare($sql);
         $prepare->execute($pxeboot, $host);
   };
   if($@){
      if($@ =~ /TARGET/){
         die "Error: $pxeboot target does not exist.\n";
      }
      if($@ =~ $dbh->err){
         die "Database Error: Failed to enable PXE for $host $@.\n";
      }
   }
  
   $self;

}

##############################################
sub disable
##############################################
{
   my( $self, $host ) = @_;
   
   # Set the device to localboot.
   
   unless($host){
      die "Error: Hostname cannot be null.\n";
   }
   $host =~ tr/[a-z]/[A-Z]/;
   
   my $dbh = $self->{_DB};
   eval {
      my $prepare = $dbh->prepare("UPDATE pxehosts SET status=2,target='localboot',disabled=CURRENT_TIMESTAMP,synced1='N',synced2='N',synced3='N', syncedlgcy1='N',syncedlgcy2='N',syncedlgcy3='N' WHERE hostname = ?");
         $prepare->execute($host);
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to disable PXE for $host $@.\n";
      }
   }

   $self;
}

##############################################
sub disablelgcy
##############################################
{
   my( $self, $host ) = @_;
      
   unless($host){
      die "Error: Hostname cannot be null.\n";
   }
   $host =~ tr/[a-z]/[A-Z]/;
   
   my $dbh = $self->{_DB};
   eval {
      my $prepare = $dbh->prepare("UPDATE pxehosts SET lgcy=0,syncedlgcy1='N',syncedlgcy2='N',syncedlgcy3='N', synced1='N', synced2='N', synced3='N' WHERE hostname = ?");
         $prepare->execute($host);
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to disable legacy boot for $host $@.\n";
      }
   }

   $self;

}

##############################################
sub enablelgcy
##############################################
{
   my( $self, $host ) = @_;
      
   unless($host){
      die "Error: Hostname cannot be null.\n";
   }
   $host =~ tr/[a-z]/[A-Z]/;
   
   my $dbh = $self->{_DB};
   eval {
      my $prepare = $dbh->prepare("UPDATE pxehosts SET lgcy=1,syncedlgcy1='N',syncedlgcy2='N',syncedlgcy3='N', synced1='N', synced2='N', synced3='N' WHERE hostname = ?");
         $prepare->execute($host);
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to enable legacy boot for $host $@.\n";
      }
   }

   $self;

}

##############################################
sub changetobios
##############################################
{
   my( $self, $host ) = @_;

   unless($host){
      die "Error: Hostname cannot be null.\n";
   }
   $host =~ tr/[a-z]/[A-Z]/;

   my $dbh = $self->{_DB};
   eval {
      my $prepare = $dbh->prepare("UPDATE pxehosts SET type=0, syncedlgcy1='N',syncedlgcy2='N',syncedlgcy3='N', synced1='N', synced2='N', synced3='N' WHERE hostname = ?");
         $prepare->execute($host);
   };
   if($@){
      if($@ =~ $dbh->err){
         die "Database Error: Failed to changetobios for $host $@.\n";
      }
   }

   $self;
}

##############################################
sub build
##############################################
{

   my( $self, $host, $pxeboot ) = @_;  
  
   # Create (pxelinux|elilo) configurations.

   $self;
}

##############################################
sub destroy
##############################################
{
   my( $self, $host ) = @_;

   # Remove PXE configurations from pxelinux.cfg/

   $self;

}
