Name: aims2
Version: 2.45
Release: 1%{?dist}
Summary: AIMS2 - Automated Installation Management Server (v.2)
Group: System Environment/Daemons
Source0: %{name}-%{version}.tgz
License: GPL
Vendor: CERN
Packager: Linux.Support@cern.ch
URL: http://cern.ch/linux/
BuildArch: noarch

%description
AIMS2 : Automated Installation Management Server (v.2)
Documentation at: http://twiki.cern.ch/twiki/bin/view/LinuxSupport/Aims2

# Only build aims2-server for el7, as we do not have all dependencies for el8
%package -n aims2-server
Summary: AIMS2 server of Automated Installation Management Server (v.2)
Group: System Environment/Daemons

%if 0%{?rhel} == 8
%define oravers 19.9
%endif%

Requires: perl >= 5.8.5
Requires: perl-SOAP-Lite >= 0.65
Requires: perl-Authen-Krb5 >= 1.5
Requires: perl-Net-DNS >= 0.48
Requires: perl-DBI >= 1.40
Requires: perl-DBD-Oracle >= 1.16
Requires: perl-LDAP >= 0.31
Requires: tftp-server >= 0.39
Requires: perl-DateManip >= 5.42
Requires: perl-SQL-Abstract >= 1.22
%if 0%{?rhel} == 8
Requires: oracle-instantclient%{oravers}-basic
%else%
Requires: oracle-instantclient-basic >= 10.2.0.3
%endif%
Requires: httpd >= 2.0.52
Requires: perl-IO-Socket-INET6
Requires: perl-Socket6
Requires: perl-CGI
Requires: perl-String-Random >= 0.25
Requires: perl-File-Find-Rule >= 0.33

Requires: systemd
BuildRequires:    systemd
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

Provides: aims2server = %{version}
Obsoletes:aims2server < %{version}


#until we rename modules correctly ...
Provides: perl(aims2server::conf), perl(aims2server::db), perl(aims2server::host), perl(aims2server::image), perl(aims2server::landb), perl(aims2server::ldap), perl(aims2server::pxe), perl(aims2server::server), perl(aims2server::soap), perl(aims2server::winsrvc)

%description -n aims2-server
Server-side software for AIMS2 remote installations service.

%package -n aims2-client
Summary: AIMS2 client of Automated Installation Management Server (v.2)
Group: System Environment/Base

Requires:  perl >= 5.8.5
Requires:  perl-SOAP-Lite >= 0.65
Requires:  perl-Authen-Krb5 >= 1.5
Requires:  perl-Net-DNS >= 0.48
Provides:  aims2client = %{version}
Obsoletes: aims2client < %{version}
Requires:  perl-Socket6
Requires:  perl-IO-Socket-INET6

%description -n aims2-client
Client-side software for using the AIMS2 remote installations service.
See: http://twiki.cern.ch/twiki/bin/view/LinuxSupport/Aims2
for more information.

%prep

%setup

%build

%install
pod2man aims2client > aims2client.1
mkdir -p %{buildroot}/usr/{bin,sbin}/
mkdir -p %{buildroot}/usr/share/man/man1/
# man entries need to be prepared so rpmbuild does not complain about aims2 man entry missing
ln -sf aims2.1 %{buildroot}/usr/share/man/man1/aims2client.1
install -p -m 644 aims2client.1 %{buildroot}/usr/share/man/man1/
# Only install aims2-server components for el7, as we do not have all dependencies for el8 build
MODULES="conf.pm db.pm host.pm image.pm landb.pm ldap.pm log.pm pxe.pm server.pm soap.pm www.pm"
CGIS="ks.cgi reboot.cgi server.cgi upload.cgi"
mkdir -p %{buildroot}/var/log/aims/
mkdir -p %{buildroot}/usr/share/perl5/aims2server/
mkdir -p %{buildroot}/var/www/cgi-bin/aims2server/
mkdir -p %{buildroot}/etc/httpd/conf.d/
mkdir -p %{buildroot}/usr/lib/systemd/system/
mkdir -p %{buildroot}/etc/logrotate.d/
mkdir -p %{buildroot}/var/lock/subsys/aims
install -p -m 755 aims2config %{buildroot}/usr/sbin/
install -p -m 755 aims2sync %{buildroot}/usr/sbin/
for CGI in $CGIS; do \
    install -p -m 755 cgi/$CGI %{buildroot}/var/www/cgi-bin/aims2server/; \
done
for MODULE in $MODULES; do \
    install -p -m 644 modules/$MODULE %{buildroot}/usr/share/perl5/aims2server/; \
done
install -p -m 644 cfg/aims2httpd.conf %{buildroot}/etc/httpd/conf.d/
install -p -m 644 cfg/aims2sync.service %{buildroot}/usr/lib/systemd/system/
install -p -m 644 cfg/aims2.conf %{buildroot}/etc/
install -p -m 644 cfg/aims2sync.logrotate %{buildroot}/etc/logrotate.d/aims2sync
ln -sf aims2client %{buildroot}/usr/bin/aims2
install -p -m 755 aims2client %{buildroot}/usr/bin/
cd %{buildroot}/usr/share/man/man1
ln -s aims2client.1.gz aims2.1.gz
cd -

%post
%systemd_post aims2sync.service

%preun
%systemd_preun aims2sync.service

%postun
%systemd_postun_with_restart aims2sync.service

# Only install aims2-server components for el7, as we do not have all dependencies for el8 build
%files -n aims2-server
%defattr(-, root, root)
/usr/sbin/aims2sync
/usr/sbin/aims2config
#/etc/init.d/aims2sync
%{_unitdir}/aims2sync.service
/etc/logrotate.d/aims2sync
%config(noreplace) /etc/aims2.conf
%config(noreplace) /etc/httpd/conf.d/aims2httpd.conf
/usr/share/perl5/aims2server/*.pm
#/usr/lib/perl5/site_perl/aims2server/*.pm
/var/www/cgi-bin/aims2server/*.cgi
%dir /var/log/aims/

%files -n aims2-client
%defattr(-, root, root)
/usr/bin/aims2
/usr/bin/aims2client
/usr/share/man/man1/*

%changelog

* Wed Dec 02 2020 Daniel Juarez <djuarezg@cern.ch> - 2.45-1
- Add perl-CGI dependency por C8

* Wed Nov 18 2020 Daniel Juarez <djuarezg@cern.ch> - 2.44-1
- Build server components for C8 as well

* Wed Nov 18 2020 Daniel Juarez <djuarezg@cern.ch> - 2.43-1
- Use linuxefi module instead of linux, LOS-627

* Fri Oct 16 2020 Daniel Juarez <djuarezg@cern.ch> - 2.42-1
- Disable autocommit on pxe conf to reduce DB transaction times

* Fri Sep 25 2020 Daniel Juarez <djuarezg@cern.ch> - 2.41-1
- Standarise grub linux and initrd directives after grub2 update

* Fri Sep 25 2020 Daniel Juarez <djuarezg@cern.ch> - 2.40-1
- Only update DB iface status when removing pxeconfs

* Fri Sep 18 2020 Daniel Juarez <djuarezg@cern.ch> - 2.39-1
- Reduce unnecessary long delays on aims2sync

* Tue Sep 8 2020 Daniel Juarez <djuarezg@cern.ch> - 2.38-1
- Split pxeboot and pxeconf building into two processes

* Mon Sep 7 2020 Daniel Juarez <djuarezg@cern.ch> - 2.37-1
- Grub UEFI conf building was wrongly comparing archs

* Fri Aug 7 2020 Daniel Juarez <djuarezg@cern.ch> - 2.36-1
- Update man content, and fix man symlinks

* Wed Aug 5 2020 Daniel Juarez <djuarezg@cern.ch> - 2.35-1
- C8 client was breaking ai-tools workflow

* Fri Jul 24 2020 Daniel Juarez <djuarezg@cern.ch> - 2.34-1
- Do image uploads through HTTP to avoid memory spikes

* Mon Jul 6 2020 Daniel Juarez <djuarezg@cern.ch> - 2.33-1
- C8 aims2-client

* Mon Jun 15 2020 Daniel Juarez <djuarezg@cern.ch> - 2.32-1
- Add armv7 and armv8 archs
- Bugfix: initrd img artifact is again optional
- Bugfix: checking image existence should not use arch nor uefi mode

* Thu Dec 05 2019 Daniel Juarez <djuarezg@cern.ch> - 2.31-1
- Adding PXEBoot media was ignoring egroups

* Mon Nov 18 2019 Daniel Juarez <djuarezg@cern.ch> - 2.30-1
- Use MIME attachments for image uploading to reduce memory usage

* Mon Nov 04 2019 Daniel Juarez <djuarezg@cern.ch> - 2.29-3
- Check PXE images existence before uploading

* Fri Nov 01 2019 Daniel Juarez <djuarezg@cern.ch> - 2.29-2
- Add building 6045 to the datacentre list for LHCb container

* Fri Nov 01 2019 Daniel Juarez <djuarezg@cern.ch> - 2.29
- Make UEFI images download through HTTP

* Wed Sep 18 2019 Daniel Abad <d.abad@cern.ch> - 2.28
- Increase initrd limit from 0.5 to 1.0 GB

* Tue May 07 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.27-2
- Remove aims2client future
- Fix global hostname override

* Fri May 03 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.27-1
- Add building 773 to the datacentre list

* Thu Apr 11 2019 Ulrich Schwickerath <ulrich.schwickerath@cern.ch> - 2.26
- improve support for IPv6 installations

* Tue Feb 19 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.25
- fix cleanup. Re-enable http:// kickstart location

* Fri Aug 17 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.20
- port server to CentOS 7
- change system to use 3 sync hosts.
- change server alias
- move to systemd

* Thu Aug 03 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.16
- add check for LandbManagerPerson for host owners.

* Thu Apr 21 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.15
- add uefi/SLC6/RHEL6 booting possibility

* Tue Apr 05 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.13.4
- recursive e-group membership search

* Wed Mar 02 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.13.1
- added uefi/aarch64 support for pxe images.

* Tue Mar 01 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.13
- added arm64 boot support
- added addhost command options for uefi/lgcy/arm64

* Fri Feb 26 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.11
- bios/uefi/lgcy boot with dnsmasq support

* Tue Feb 23 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.10-1
- adding bios/uefi/lgcy boot.

* Mon Jan 18 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.22-1
- change boot protocol to http (aims2sync)

* Thu Jan 14 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.21-1
- image upload permissions for CMF admins. (server)

* Mon Jul 20 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.19-2
- fix actual perl-Socket6 and perl-IO-Socket-INET6 requires in
  aims2-client package.

* Thu Apr 30 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.19-1
- add perl-Socket6 and perl-IO-Socket-INET6 as deoendencies
  to make the client work on IPv6-enabled systems too.
- (will still use IPv4)

* Wed Jan  7 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.17
- CDB checks in server disabled, quattor is off since nov 2014
* Fri Mar 15 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.16
- display single sync server status in client '--full' option
* Wed Apr  4 2012   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.14
- added nested e-groups support for host permission checking.
* Mon Apr  4 2011  Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.9
- added pxe noexpiry option
* Thu Oct 21 2010  KELEMEN Peter <Peter.Kelemen@cern.ch> - 2.9.6-1
- fix CDBSQL SQL statement in preparation for using dedicated ITCORE account

* Fri Aug 27 2010  KELEMEN Peter <Peter.Kelemen@cern.ch> - 2.9.5-1
- error handling fixes
- added logrotate
