# AIMS2: Automated Installation Management System (v. 2)

**IMPORTANT: YOU WILL NEED TO COPY RPMS TO `/mnt/data2/home/build/packages/incoming/cc7/cern` SO THEY BECOME AVAILABLE ON THE [`CERN`](http://linuxsoft.cern.ch/cern/centos/7/cern) REPO**


Ref. <https://linux.web.cern.ch/installation/aims/aims2client/>

Create DB schema:


```
create table conf ( key varchar2(32) not null primary key, value varchar2(400) not null);


create table hardware ( hostname varchar2(100) not null , hw varchar2(32) not null );
alter table hardware add constraint HW_PK unique (hw);

create table kickstart ( hostname varchar2(100) not null , ks clob, source varchar2(500) not null, lastupdated date, username varchar2(32) );

create table owners ( owner varchar2(100) not null , egroups varchar2(100) not null );

create table pxeboot ( name varchar2(100) not null, arch varchar2(12), description varchar2(500), owner varchar2(100), initrd_source varchar2(500), initrd_sum varchar2(32), initrd_size number(10), initrd_file blob, vmlinuz_source varchar2(500), vmlinuz_sum varchar2(32), vmlinuz_size number(10), vmlinuz_file blob, kopts varchar2(500), uploaded date, groups varchar2(100), synced1 char(1), synced2 char(1), synced3 char(1), uefi number(1));
alter table pxeboot add constraint BOOT_PK unique (name);

create table pxehosts ( hostname varchar2(100) not null, status number(1) not null, target varchar2(100) not null, username varchar2(100) not null, kopts varchar2(500), registered date, enabled date, booted date, disabled date, synced1 char(1), synced2 char(1), synced3 char(1), noexpiry number(38), type number(1), lgcy number(1), syncedlgcy1 char(1), syncedlgcy2 char(1), syncedlgcy3 char(1) );
alter table pxehosts add constraint HOST_PK unique (hostname);

DB schema:

SQL> select table_name from user_tables;

TABLE_NAME
------------------------------
CONF
HARDWARE
KICKSTART
OWNERS
PXEBOOT
PXEHOSTS

SQL> desc conf
 Name					   Null?    Type
 ----------------------------------------- -------- ----------------------------
 KEY					   NOT NULL VARCHAR2(32)
 VALUE					   NOT NULL VARCHAR2(400)

SQL> desc hardware
 Name					   Null?    Type
 ----------------------------------------- -------- ----------------------------
 HOSTNAME				   NOT NULL VARCHAR2(100)
 HW					   NOT NULL VARCHAR2(32)

SQL> desc kickstart
 Name					   Null?    Type
 ----------------------------------------- -------- ----------------------------
 HOSTNAME				   NOT NULL VARCHAR2(100)
 KS						    CLOB
 SOURCE 				   NOT NULL VARCHAR2(500)
 LASTUPDATED					    DATE
 USERNAME					    VARCHAR2(32)

SQL> desc owners
 Name					   Null?    Type
 ----------------------------------------- -------- ----------------------------
 OWNER					   NOT NULL VARCHAR2(100)
 EGROUPS				   NOT NULL VARCHAR2(100)

SQL> desc pxeboot;
 Name					   Null?    Type
 ----------------------------------------- -------- ----------------------------
 NAME					   NOT NULL VARCHAR2(100)
 ARCH						    VARCHAR2(12)
 DESCRIPTION					    VARCHAR2(500)
 OWNER						    VARCHAR2(100)
 INITRD_SOURCE					    VARCHAR2(500)
 INITRD_SUM					    VARCHAR2(32)
 INITRD_SIZE					    NUMBER(10)
 INITRD_FILE					    BLOB
 VMLINUZ_SOURCE 				    VARCHAR2(500)
 VMLINUZ_SUM					    VARCHAR2(32)
 VMLINUZ_SIZE					    NUMBER(10)
 VMLINUZ_FILE					    BLOB
 KOPTS						    VARCHAR2(500)
 UPLOADED					    DATE
 GROUPS 					    VARCHAR2(100)
 SYNCED1					    CHAR(1)
 SYNCED2					    CHAR(1)
 SYNCED3					    CHAR(1)
 UEFI						    NUMBER(1)

SQL> desc pxehosts;         
 Name					   Null?    Type
 ----------------------------------------- -------- ----------------------------
 HOSTNAME				   NOT NULL VARCHAR2(100)
 STATUS 				   NOT NULL NUMBER(1)
 TARGET 				   NOT NULL VARCHAR2(100)
 USERNAME				   NOT NULL VARCHAR2(100)
 KOPTS						    VARCHAR2(500)
 REGISTERED					    DATE
 ENABLED					    DATE
 BOOTED 					    DATE
 DISABLED					    DATE
 SYNCED1					    CHAR(1)
 SYNCED2					    CHAR(1)
 SYNCED3					    CHAR(1)
 NOEXPIRY					    NUMBER(38)
 TYPE						    NUMBER(1)
 LGCY						    NUMBER(1)
 SYNCEDLGCY1					    CHAR(1)
 SYNCEDLGCY2					    CHAR(1)
 SYNCEDLGCY3                                        CHAR(1)
```

